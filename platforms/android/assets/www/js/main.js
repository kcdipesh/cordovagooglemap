//to detect whether we are inside mobile webview or in browser (for testing), 
//true means we are inside the cordova webview
var isPhoneGap = (document.URL.indexOf('http://') === -1 && document.URL.indexOf('https://') === -1);

//the latitude and longitude to center the map on and show marker
var currLat = 37.7906189;
var currLng = -122.3951257;

//fired on body load
function onBodyLoad() {
    if (isPhoneGap) {
        document.addEventListener('deviceready', onDeviceReady, false);    
    } else {
        onDeviceReady();
    }
}

//this function is called when the mobile device is ready when inside cordova webview 
function onDeviceReady() {
        renderMap();
        document.addEventListener("offline", function() {
            alert('No internet');
        }, false);
}

function renderMap() {
    var californiaLatLng = new google.maps.LatLng(currLat, currLng); 
    $('#map_canvas').gmap({ 'center': californiaLatLng });
    $('#map_canvas').gmap('addMarker', { /*id:'m_1',*/ 'position': californiaLatLng, 'bounds': true } );
}